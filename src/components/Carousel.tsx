import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Scrollbar } from "swiper";
import { HiChevronDown } from "react-icons/hi";
import "swiper/css";
import "swiper/css/scrollbar";

export default function Carousel() {
  return (
    <>
      <Swiper
        className="w-full h-screen"
        direction={"vertical"}
        scrollbar={{ hide: true }}
        modules={[Scrollbar]}
      >
        <SwiperSlide className="bg-cover bg-center bg-[url('./assets/3304be3b-dd0a-4128-9c26-eb61c0b98d61.jpeg')]">
          {({ isActive }) => (
            <div
              className={`flex flex-col items-center mt-28 ${
                isActive ? "animate-fadeIn" : "animate-fadeOut"
              }`}
            >
              <span className="[animation-delay:0.3s] opacity-0 text-4xl font-semibold tracking-wide mb-2 animate-translateEffectDownToTop">
                Model Y
              </span>
              <a
                href="link"
                className="font-light [animation-delay:1s] opacity-0 underline underline-offset-4 animate-translateEffectDownToTop"
              >
                En savoir plus sur Tesla for Business
              </a>

              <div className="flex flex-col mt-[26rem] mx-6">
                <button className="bg-[#171A20CC] opacity-0 [animation-delay:1s] py-2 px-20 text-white rounded font-semibold mb-4 animate-translateEffectDownToTop">
                  Configuration personnalisée
                </button>
                <button className="text-[#171A20CC] opacity-0 [animation-delay:1s] py-2 px-20 bg-white rounded font-semibold animate-translateEffectDownToTop">
                  Essais
                </button>
                <div className="animate-translateEffectTopToDown [animation-delay:1.5s] opacity-0 relative top-[-1rem]">
                  <p className="text-center text-sm">5 étoiles au crash-test Euro NCAP</p>
                  <HiChevronDown className="w-8 h-8 flex m-auto animate-bounceArrow" />
                </div>
              </div>
            </div>
          )}
        </SwiperSlide>
        <SwiperSlide className="bg-cover bg-center bg-[url('./assets/M3-Homepage-Mobile-LHD.jpeg')]">
          {({ isActive }) => (
            <div
              className={`flex flex-col items-center mt-28 ${
                isActive ? "animate-fadeIn" : "animate-fadeOut"
              }`}
            >
              <span className="text-4xl font-semibold tracking-wide mb-2">Model 3</span>
              <a href="link" className="font-light underline underline-offset-4">
                Réservez votre essai
              </a>

              <div className="flex flex-col mt-[26rem] mx-6">
                <button className="bg-[#171A20CC] py-2 px-12 text-white rounded font-semibold mb-4">
                  Configuration personnalisée
                </button>
                <button className="text-[#171A20CC] py-2 px-12 bg-white rounded font-semibold">
                  Découvrir nos véhicules disponibles
                </button>
                <p className="text-center text-sm mt-4">
                  A reçu la note maximale de 5 étoiles par Euro NCAP
                </p>
              </div>
            </div>
          )}
        </SwiperSlide>
        <SwiperSlide className="bg-cover bg-center bg-[url('./assets/Homepage-Model-S-Mobile-LHD.jpeg')]"></SwiperSlide>
        <SwiperSlide className="bg-cover bg-center bg-[url('./assets/Homepage-Model-X-Mobile-LHD.jpeg')]"></SwiperSlide>
        <SwiperSlide className="bg-cover bg-center bg-[url('./assets/4a2dfc25-8931-4aae-aa41-96563261d221.jpeg')]"></SwiperSlide>
        <SwiperSlide className="bg-cover bg-center bg-[url('./assets/asset5.jpeg')]"></SwiperSlide>
      </Swiper>
    </>
  );
}
