import React, { Fragment } from "react";
import { Menu, Dialog, Transition } from "@headlessui/react";
import { HiOutlineX } from "react-icons/hi";

export default function NavBar() {
  return (
    <>
      <nav className="fixed w-full flex z-10 justify-between mt-4">
        <svg
          className="w-32 ml-4 tds-icon tds-icon-logo-wordmark tds-site-logo-icon"
          viewBox="0 0 342 35"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M0 .1a9.7 9.7 0 0 0 7 7h11l.5.1v27.6h6.8V7.3L26 7h11a9.8 9.8 0 0 0 7-7H0zm238.6 0h-6.8v34.8H263a9.7 9.7 0 0 0 6-6.8h-30.3V0zm-52.3 6.8c3.6-1 6.6-3.8 7.4-6.9l-38.1.1v20.6h31.1v7.2h-24.4a13.6 13.6 0 0 0-8.7 7h39.9v-21h-31.2v-7h24zm116.2 28h6.7v-14h24.6v14h6.7v-21h-38zM85.3 7h26a9.6 9.6 0 0 0 7.1-7H78.3a9.6 9.6 0 0 0 7 7zm0 13.8h26a9.6 9.6 0 0 0 7.1-7H78.3a9.6 9.6 0 0 0 7 7zm0 14.1h26a9.6 9.6 0 0 0 7.1-7H78.3a9.6 9.6 0 0 0 7 7zM308.5 7h26a9.6 9.6 0 0 0 7-7h-40a9.6 9.6 0 0 0 7 7z"
            fill="currentColor"
          ></path>
        </svg>

        <Menu as="div" className="relative z-50">
          <Menu.Button className="bg-opacity-bg py-1 px-4 mr-4 rounded">
            <p className="font-semibold text-black">Menu</p>
          </Menu.Button>
          <Transition.Root>
            <Transition.Child
              as={Fragment}
              enter="transition ease-out duration-300"
              enterFrom="transform opacity-0"
              enterTo="transform opacity-100"
              leave="transition ease-in duration-200"
              leaveFrom="transform opacity-100"
              leaveTo="transforme opacity-0"
            >
              <div className="fixed inset-0 backdrop-blur bg-opacity-75" />
            </Transition.Child>
            <Transition.Child
              as={Fragment}
              enter="transition ease-in-out duration-500"
              enterFrom="transform opacity-0 -translate-x-0 "
              enterTo="transform opacity-100 -translate-x-16 right-[-4rem]"
              leave="transition ease-in-out duration-500"
              leaveFrom="transform opacity-100 -translate-x-16 right-[-4rem]"
              leaveTo="transform opacity-0 -translate-x-0 right-[-4rem]"
            >
              <Dialog
                as="div"
                className="p-8 w-[75%] h-screen bg-white fixed z-50 top-0 right-[-4rem]"
                onClose={() => ""}
              >
                <Dialog.Title>
                  <HiOutlineX className="w-6 h-6 absolute right-8" />
                </Dialog.Title>
                <Dialog.Panel>
                  <Menu.Items className="flex flex-col mt-14">
                    <Menu.Item className="mb-4" as="a">
                      Model S
                    </Menu.Item>
                    <Menu.Item className="mb-4" as="a">
                      Model 3
                    </Menu.Item>
                    <Menu.Item className="mb-4" as="a">
                      Model X
                    </Menu.Item>
                    <Menu.Item className="mb-4" as="a">
                      Model Y
                    </Menu.Item>
                    <Menu.Item className="mb-4" as="a">
                      Véhicules disponibles
                    </Menu.Item>
                    <Menu.Item className="mb-4" as="a">
                      Véhicules D'occasion
                    </Menu.Item>
                  </Menu.Items>
                </Dialog.Panel>
              </Dialog>
            </Transition.Child>
          </Transition.Root>
        </Menu>
      </nav>
    </>
  );
}
