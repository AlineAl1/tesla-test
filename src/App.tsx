import React from "react";
import Carousel from "./components/Carousel";
import NavBar from "./components/NavBar";

function App() {
  return (
    <>
      <NavBar />
      <Carousel />
    </>
  );
}

export default App;
