module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        "opacity-bg": "rgba(0, 0, 0, 0.05)",
      },
      keyframes: {
        translateEffectDownToTop: {
          "0%": {
            transform: "translateY(-0%)",
            opacity: "0",
          },
          "100%": {
            transform: "translateY(-40%)",
            opacity: "1",
          },
        },
        translateEffectTopToDown: {
          "0%": {
            transform: "translateY(0%)",
            opacity: "0",
          },
          "100%": {
            transform: "translateY(40%)",
            opacity: "1",
          },
        },
        bounceArrow: {
          "0%": {
            transform: "translateY(0%)",
            opacity: "1",
          },
          "25%": {
            transform: "translateY(20%)",
          },
          "50%": {
            transform: "translateY(0%)",
          },
          "75%": {
            transform: "translateY(10%)",
          },
          "100%": {
            transform: "translateY(0%)",
            opacity: "1",
          },
        },
        fadeIn: {
          "0%": { opacity: 0 },
          "100%": { opacity: 1 },
        },
        fadeOut: {
          "0%": { opacity: 1 },
          "100%": { opacity: 0 },
        },
      },
      animation: {
        translateEffectDownToTop: "translateEffectDownToTop 0.7s ease-in-out forwards",
        translateEffectTopToDown: "translateEffectTopToDown 0.7s ease-in-out forwards",
        bounceArrow: "bounceArrow 2.5s ease-in-out infinite",
        fadeIn: "fadeIn 0.8s ease-in-out forwards",
        fadeOut: "fadeOut 0.8s ease-in-out forwards",
      },
    },
  },
  plugins: [],
};
